T-Connect
---------
 *Android application for connecting users under the same telecom company in Tanzania*
  
Changelog:

T-Connect 1.02 Beta version:
- Some minor fixes
- Some redesign
- Implemented our own backend
- Remove any use of Parse as the Backend

Note: The account settings if still a work in progress...

> Written by [Yusuph Wickama](https://gitlab.com/yusuphwickama)