package com.wickerlabs.tconnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.DigitsAuthButton;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.wickerlabs.tconnect.backend.Backend;


public class WelcomeScreen extends AppCompatActivity {

    String TAG = "HaloConnect";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        final boolean init = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).getBoolean("init", false);
        final boolean regCompleted = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).getBoolean("regCompleted", false);
        final boolean verified = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).getBoolean("verified", false);
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            Log.i(TAG, "No actionBar available");
        }

        DigitsAuthButton digitsButton = (DigitsAuthButton) findViewById(R.id.auth_button);
        assert digitsButton != null;
        digitsButton.setText("Let's get started");
        digitsButton.setTextColor(getResources().getColor(R.color.coolYellow));
        digitsButton.setAuthTheme(R.style.CustomDigitsTheme);
        digitsButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.welcome_btn));
        digitsButton.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Toast.makeText(getApplicationContext(), "Verification successful", Toast.LENGTH_LONG).show();

                getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).edit()
                        .putString("phoneNum", phoneNumber)
                        .putBoolean("verified", true).apply();

                handleTranstition(true, regCompleted, init);

            }

            @Override
            public void failure(DigitsException exception) {
                Toast.makeText(getApplicationContext(), "Verification failed", Toast.LENGTH_LONG).show();
                Log.d("Digits", "Sign in with Digits failure", exception);
            }
        });

        handleTranstition(verified, regCompleted, init);


    }

    private void handleTranstition(boolean verified, boolean regCompleted, boolean init) {
        if (verified) {

            if (init && regCompleted) {
                Intent intent = new Intent(WelcomeScreen.this, UserPanel.class);
                startActivity(intent);
                WelcomeScreen.this.finish();
            } else if (!regCompleted) {
                Intent intent = new Intent(WelcomeScreen.this, FinalDetails.class);
                startActivity(intent);
                WelcomeScreen.this.finish();
            } else {
                Intent intent = new Intent(WelcomeScreen.this, Initializing.class);
                startActivity(intent);
                WelcomeScreen.this.finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Backend.getInstance().getCurrentUser() != null) {
            WelcomeScreen.this.finish();
        }
    }
}
