package com.wickerlabs.tconnect;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.callback.UploadCallback;
import com.wickerlabs.tconnect.util.ValidUtils;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Initializing extends AppCompatActivity {

    private static String AUTHORITY;
    private static String ACCOUNT_TYPE;
    String TAG = "Initialize";
    Context context;
    Account mAcount;
    int count = 0, numCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initializing);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        final TextView initText = (TextView) findViewById(R.id.initTxt);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(count <= 2){
                            initText.setText(initText.getText().toString().concat("."));
                            count++;
                        } else {
                            initText.setText(getString(R.string.initializing));
                            count = 0;
                        }
                    }
                });
            }
        }, 1000, 1000);

        context = getApplicationContext();
        ACCOUNT_TYPE = getString(R.string.account_type);

        uploadContacts();
    }

    public void loadMainUserPanel() {
        Answers.getInstance().logCustom(new CustomEvent("Initialize"));
        Intent intent = new Intent(Initializing.this, UserPanel.class);
        startActivity(intent);
        Initializing.this.finish();
    }


    private void uploadContacts() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "[+] Android version " + Build.VERSION.RELEASE);
                final ArrayList<String> phoneNumbers = new ArrayList<>();
                int lastCount = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getInt("ct_count", 0);
                Log.d(TAG, "[+] Initial count -> " + String.valueOf(lastCount));

                try {
                    ContentResolver cr = context.getContentResolver();
                    Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                    final Cursor numbers = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);


                    if (numbers != null) {
                        numCount = numbers.getCount();
                        Log.d(TAG, "[+] Current count -> " + String.valueOf(numCount));
                    } else {
                        Log.d(TAG, "[+] Current count -> NULL");
                    }

                    if (cur != null && numbers != null && cur.getCount() > 0 && numCount != lastCount) {

                        Log.d(TAG, "[+] Contacts changed");
                        Log.d(TAG, "[+] Reading new contact numbers...");

                        while (cur.moveToNext()) {
                            String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                            Cursor phoneCursor;

                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                                phoneCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                            } else {
                                phoneCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " = 1 AND " + ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                            }

                            if (phoneCursor != null) {
                                while (phoneCursor.moveToNext()) {
                                    String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    // Get phone numbers from device and add to database
                                    if (ValidUtils.isPhoneValid(phoneNumber) && !phoneNumbers.contains(ValidUtils.standardTZNumber(phoneNumber))) {
                                        phoneNumbers.add(ValidUtils.standardTZNumber(phoneNumber));
                                    }
                                }
                                phoneCursor.close();
                            }
                        }

                        cur.close();
                        numbers.close();

                        Backend.getInstance().uploadContacts(phoneNumbers, new UploadCallback() {
                            @Override
                            public void onUploadComplete(String message, Exception e) {
                                if (e == null) {
                                    Log.d(TAG, "[+] Uploading complete");
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                    context.getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).edit()
                                            .putBoolean("init", true)
                                            .putInt("ct_count", numCount)
                                            .apply();
                                    loadMainUserPanel();
                                } else {
                                    Log.d(getClass().getSimpleName(), "[+] Upload failed", e);
                                }
                            }
                        });
                    } else {
                        Log.d(TAG, "[+] No change, skipping upload");
                    }

                    Log.d(TAG, "[+] numbers read -> " + phoneNumbers.size());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
