package com.wickerlabs.tconnect.backend;

import com.wickerlabs.tconnect.backend.callback.AddCallback;
import com.wickerlabs.tconnect.backend.callback.GetCallback;
import com.wickerlabs.tconnect.backend.callback.RegistrationCallback;
import com.wickerlabs.tconnect.backend.callback.UploadCallback;

import java.util.List;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 2:55 PM
 * Date: 2/8/2017
 * Website: http://www.wickerlabs.com
 */

interface CustomBackend {

    void registerUser(WickerUser user, RegistrationCallback registrationCallback);

    WickerUser getCurrentUser();

    void addOtherNumber(List<String> otherNumber, AddCallback addCallback);

    void uploadContacts(List<String> contacts, UploadCallback callback);

    void findMyContacts(GetCallback callback);


}
