package com.wickerlabs.tconnect.backend.callback;

import com.wickerlabs.tconnect.backend.Friend;

import java.util.List;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 11:51 PM
 * Date: 2/12/2017
 * Website: http://www.wickerlabs.com
 */

public interface GetCallback {

    void onComplete(List<Friend> users, Exception e);
}
