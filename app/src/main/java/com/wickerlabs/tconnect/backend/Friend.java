package com.wickerlabs.tconnect.backend;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 1:26 PM
 * Date: 2/15/2017
 * Website: http://www.wickerlabs.com
 */

public class Friend {
    private String uid, name, phoneNum, privacy, avatar;
    private String otherOne;
    private String otherTwo;

    Friend() {
    }

    public String getOtherOne() {
        return otherOne;
    }

    public Friend setOtherOne(String otherOne) {
        this.otherOne = otherOne;
        return this;
    }

    public String getOtherTwo() {
        return otherTwo;
    }

    public Friend setOtherTwo(String otherTwo) {
        this.otherTwo = otherTwo;
        return this;
    }

    public String getUid() {
        return uid;
    }


    public Friend setUid(String uid) {
        this.uid = uid;
        return this;
    }


    public String getName() {
        return name;
    }


    public Friend setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public Friend setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
        return this;
    }

    public String getPrivacy() {
        return privacy;
    }


    public Friend setPrivacy(String privacy) {
        this.privacy = privacy;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public Friend setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }
}
