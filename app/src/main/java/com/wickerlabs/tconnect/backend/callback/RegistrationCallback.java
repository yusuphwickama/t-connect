package com.wickerlabs.tconnect.backend.callback;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 11:51 PM
 * Date: 2/12/2017
 * Website: http://www.wickerlabs.com
 */

public interface RegistrationCallback {

    void onRegistrationComplete(String message, Exception e);
}
