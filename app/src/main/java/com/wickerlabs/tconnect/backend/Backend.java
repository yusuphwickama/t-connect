package com.wickerlabs.tconnect.backend;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.wickerlabs.tconnect.BuildConfig;
import com.wickerlabs.tconnect.R;
import com.wickerlabs.tconnect.backend.callback.AddCallback;
import com.wickerlabs.tconnect.backend.callback.GetCallback;
import com.wickerlabs.tconnect.backend.callback.RegistrationCallback;
import com.wickerlabs.tconnect.backend.callback.UploadCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 2:22 PM
 * Date: 2/12/2017
 * Website: http://www.wickerlabs.com
 */
@SuppressLint("StaticFieldLeak")
public class Backend implements CustomBackend {

    private static Backend instance;
    private Context mContext;
    private String APIKey, registration, getUser, otherNumbers, contacts;
    private WickerUser mUser;
    private ProgressDialog dialog;

    private Backend(Context context) {
        mContext = context;
        this.registration = getContext().getResources().getString(R.string.register_link);
        this.otherNumbers = getContext().getResources().getString(R.string.add_other_link);
        this.contacts = getContext().getResources().getString(R.string.upload_contacts);
        this.getUser = getContext().getString(R.string.get_contacts);
        this.APIKey = getContext().getResources().getString(R.string.server_api_key);
    }

    public static Backend getInstance() {
        return instance;
    }

    public static void with(Context context) {
        if (instance == null) {
            instance = new Backend(context);
        }
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void addOtherNumber(List<String> otherNumber, final AddCallback addCallback) {
        JSONObject contacts = new JSONObject();

        for (int i = 0; i < otherNumber.size(); i++) {
            try {
                contacts.put("other".concat(String.valueOf(i + 1)), otherNumber.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final JSONObject bundledContacts = new JSONObject();
        try {
            bundledContacts.put("contacts", contacts);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.POST, this.otherNumbers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");

                    if (message.equals("Registration complete")) {
                        addCallback.onAddComplete(message, null);
                    } else {
                        addCallback.onAddComplete(message, new Exception(jsonObject.getString("error")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adding-error", error.getLocalizedMessage());
                addCallback.onAddComplete(error.getLocalizedMessage(), error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<String, String>();
                param.put("uid", getCurrentUser().getUid());
                param.put("JSON", bundledContacts.toString());
                param.put("apiKey", Uri.encode(APIKey));
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "*/*");
                headers.put("Cache-control", "no-cache");
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 2;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
        //Send Post request
        requestQueue.add(request);
    }

    @Override
    public void uploadContacts(List<String> contacts, final UploadCallback callback) {
        JSONObject jsonContacts = new JSONObject();

        for (int i = 0; i < contacts.size(); i++) {
            try {
                jsonContacts.put("other".concat(String.valueOf(i + 1)), contacts.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final JSONObject bundledContacts = new JSONObject();
        try {
            bundledContacts.put("contacts", jsonContacts);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.POST, this.contacts, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");

                    if (message.equals("Initialization complete")) {
                        callback.onUploadComplete(message, null);
                    } else {
                        callback.onUploadComplete(message, new Exception(jsonObject.getString("error")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Upload-Error", error.getMessage());
                callback.onUploadComplete(error.getMessage(), error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("uid", getCurrentUser().getUid());
                param.put("JSON", bundledContacts.toString());
                param.put("apiKey", Uri.encode(APIKey));
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "*/*");
                headers.put("Cache-control", "no-cache");
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 2;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
        //Send Post request
        requestQueue.add(request);
    }

    @Override
    public void findMyContacts(final GetCallback callback) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.POST, this.getUser, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray users = jsonObject.getJSONArray("results");
                    List<Friend> friends = new ArrayList<>();
                    for (int i = 0; i < users.length(); i++) {
                        JSONObject user = users.getJSONObject(i);
                        Friend myFriend = new Friend()
                                .setUid(user.getString("id"))
                                .setName(user.getString("name"))
                                .setPhoneNum(user.getString("phoneNum"))
                                .setOtherTwo(user.getString("otherTwo"))
                                .setOtherOne(user.getString("otherOne"))
                                .setPrivacy(user.getString("privacy"))
                                .setAvatar(user.getString("avatar"));
                        friends.add(myFriend);
                    }
                    callback.onComplete(friends, null);

                } catch (JSONException e) {
                    callback.onComplete(null, e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(getClass().getSimpleName(), error.getLocalizedMessage());
                callback.onComplete(null, error);
                // callback call error
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("uid", getCurrentUser().getUid());
                param.put("apiKey", Uri.encode(APIKey));
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "*/*");
                headers.put("Cache-control", "no-cache");
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 2;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
        //Send Post request
        requestQueue.add(request);
    }

    @Override
    public void registerUser(final WickerUser user, final RegistrationCallback registrationCallback) {
        this.mUser = user;

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.POST, this.registration, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = new JSONObject();
                try {
                    Log.d("Response", response);

                    jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");

                    if (message.equals("Registration successful")) {
                        String uid = jsonObject.getString("userID");
                        mUser.setUid(uid);

                        String gUser = new Gson().toJson(mUser);
                        getContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
                                .edit()
                                .putString("curUser", gUser)
                                .apply();

                        registrationCallback.onRegistrationComplete(message, null);
                    } else {
                        registrationCallback.onRegistrationComplete(null, new Exception(jsonObject.getString("error")));
                    }


                } catch (JSONException e) {
                    registrationCallback.onRegistrationComplete(null, e);
                    Log.d("Error", e.getLocalizedMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                registrationCallback.onRegistrationComplete(error.getMessage(), error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<String, String>();
                param.put("fName", Uri.encode(user.getName()));
                param.put("altOne", Uri.encode(user.getAltPhone1()));
                param.put("altTwo", Uri.encode(user.getAltPhone2()));
                param.put("phoneNum", Uri.encode(user.getPhoneNumber()));
                param.put("apiKey", Uri.encode(APIKey));
                if (user.getBase64Image() != null) {
                    param.put("image", user.getBase64Image());
                }
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "*/*");
                headers.put("Cache-control", "no-cache");
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 2;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
                registrationCallback.onRegistrationComplete(null, error);
            }
        });
        //Send Post request

        requestQueue.add(request);
    }

    @Override
    public WickerUser getCurrentUser() {
        String user = getContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
                .getString("curUser", null);

        return new Gson().fromJson(user, WickerUser.class);
    }

}
