package com.wickerlabs.tconnect.backend;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 1:46 PM
 * Date: 2/15/2017
 * Website: http://www.wickerlabs.com
 */

public class ContactsUtils {

    public static String findNameByNumber(Context context, final String phoneNumber) {
        /*
        *   This method accepts a phone number and returns the name assigned to the contact
        *   with that number.
        * */
        ContentResolver cr = context.getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null);
        if (cursor == null) {
            return null;
        }

        String contactName = null;

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

    public static String findNameByNumber(Context context, Friend friend) {
        /*
        *   This method accepts a phone number and returns the name assigned to the contact
        *   with that number.
        * */
        String phoneNumber;

        if (friend.getOtherTwo() != null && isInPhonebook(context, friend.getOtherTwo())) {
            phoneNumber = friend.getOtherTwo();
        } else {
            phoneNumber = friend.getOtherOne();
        }

        ContentResolver cr = context.getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null);
        if (cursor == null) {
            return null;
        }

        String contactName = null;

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return (contactName == null || contactName.length() < 1) ? friend.getName() : contactName;
    }

    public static boolean isInPhonebook(Context context, String phoneNum) {
        /*
        *   This method checks if the phone number is in the phonebook and returns true or false.
        *
        * */
        ContentResolver cr = context.getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNum));

        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.NUMBER}, null,
                new String[]{ContactsContract.PhoneLookup.NUMBER}, null);

        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    return true;
                }
            } finally {
                cursor.close();
            }
        }

        return false;
    }

}
