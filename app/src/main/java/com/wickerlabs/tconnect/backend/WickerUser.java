package com.wickerlabs.tconnect.backend;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 2:59 PM
 * Date: 2/8/2017
 * Website: http://www.wickerlabs.com
 */

public class WickerUser {

    private String uid;
    private String name, phoneNumber, altPhone1, altPhone2;
    private String privacy;
    private String base64Image;

    public WickerUser() {

    }

    public String getBase64Image() {
        return base64Image;
    }

    public WickerUser setBase64Image(String base64Image) {
        this.base64Image = base64Image;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public WickerUser setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public WickerUser setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getPrivacy() {
        return privacy;
    }

    public WickerUser setPrivacy(String privacy) {
        this.privacy = privacy;
        return this;
    }

    public Bitmap getImage() {
        if (getBase64Image() != null) {
            byte[] base64 = Base64.decode(getBase64Image(), Base64.DEFAULT);

            return BitmapFactory.decodeByteArray(base64, 0, base64.length);
        }

        return null;
    }

    /*BEGIN OF SET METHODS*/

    public String getAltPhone1() {
        return altPhone1;
    }

    public WickerUser setAltPhone1(String altPhone1) {
        this.altPhone1 = altPhone1;
        return this;
    }

    public String getAltPhone2() {
        return altPhone2;
    }

    public WickerUser setAltPhone2(String altPhone2) {
        this.altPhone2 = altPhone2;
        return this;
    }

    public String getName() {
        return name;
    }

    public WickerUser setName(String name) {
        this.name = name;
        return this;
    }
}
