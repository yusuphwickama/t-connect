package com.wickerlabs.tconnect.fragments;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wickerlabs.tconnect.FinalDetails;
import com.wickerlabs.tconnect.R;
import com.wickerlabs.tconnect.adapters.UserList;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.Friend;
import com.wickerlabs.tconnect.backend.callback.GetCallback;
import com.wickerlabs.tconnect.util.DialogUtils;

import java.util.ArrayList;
import java.util.List;


public class UserFriendsFragment extends Fragment {

    public static UserList userListAdapter;
    ListView usersList;
    TextView found, editNumbers;
    List<Friend> friendList;
    ImageButton loading;
    RelativeLayout load;
    int skipCount = 0;

    public UserFriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_halo_friends2, container, false);

        loading = (ImageButton) view.findViewById(R.id.loadingBtn);
        load = (RelativeLayout) view.findViewById(R.id.loadRelative);

        load.setVisibility(View.GONE);

        // initiate user's list view
        usersList = (ListView) view.findViewById(R.id.userList);
        friendList = new ArrayList<>();

        editNumbers = (TextView) view.findViewById(R.id.editNumbers);

        editNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FinalDetails.class);
                startActivity(intent);
            }
        });

        userListAdapter = new UserList(getActivity(), R.layout.users_list_save, friendList);
        usersList.setAdapter(userListAdapter);

        found = (TextView) view.findViewById(R.id.found);

        getUsers();

        return view;
    }

    public void getUsers() {
        DialogUtils.getInstance().startProgress(getContext(), "Loading users...");

        Backend.getInstance().findMyContacts(new GetCallback() {
            @Override
            public void onComplete(List<Friend> users, Exception e) {
                if (e == null) {
                    if (users.size() > 0) {
                        for (Friend myFriend : users) {
                            friendList.add(myFriend);
                        }
                        found.setVisibility(View.GONE);
                        editNumbers.setVisibility(View.GONE);
                        userListAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d(getClass().getSimpleName(), "Failed to retrieve users", e);
                }

                DialogUtils.getInstance().stopProgress();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.halofragment, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem item = menu.findItem(R.id.action_refresh);
        Drawable icon = getResources().getDrawable(R.drawable.ic_refresh_white_24dp);
        //icon.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        item.setIcon(icon);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            refreshData();
        }
        return true;
    }

    private void startAnimation(final boolean start) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (start) {
                    load.setVisibility(View.VISIBLE);
                    Animation rotation = AnimationUtils.loadAnimation(getContext(), R.anim.rotation);
                    rotation.setFillAfter(true);
                    loading.startAnimation(rotation);
                } else {
                    loading.clearAnimation();
                    load.setVisibility(View.GONE);
                }
            }
        });

    }

    private void refreshData() {
        skipCount = 0;
        friendList = new ArrayList<>();
        userListAdapter = new UserList(getActivity(), R.layout.users_list_save, friendList);
        usersList.setAdapter(userListAdapter);
        getUsers();
    }

}
