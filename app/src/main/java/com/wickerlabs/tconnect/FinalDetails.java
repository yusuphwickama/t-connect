package com.wickerlabs.tconnect;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;
import com.github.kittinunf.fuel.util.Base64;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.WickerUser;
import com.wickerlabs.tconnect.backend.callback.RegistrationCallback;
import com.wickerlabs.tconnect.util.DialogUtils;
import com.wickerlabs.tconnect.util.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.wickerlabs.tconnect.util.ValidUtils.isPhoneValid;
import static com.wickerlabs.tconnect.util.ValidUtils.standardTZNumber;

public class FinalDetails extends AppCompatActivity {

    CircleImageView userPic;
    ImageButton editPic;
    Bitmap avatar;
    boolean hasPic = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_numbers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Final step");

        userPic = (CircleImageView) findViewById(R.id.userPic);

        editPic = (ImageButton) findViewById(R.id.editPicInfo);
        editPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imgIntent = new Intent(Intent.ACTION_GET_CONTENT);
                imgIntent.setType("image/*");
                startActivityForResult(imgIntent, 2123);
            }
        });

        final EditText name = (EditText) findViewById(R.id.name);
        final EditText other1 = (EditText) findViewById(R.id.other1);
        final EditText other2 = (EditText) findViewById(R.id.other2);

        other1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isPhoneValid(s.toString())) {
                    other1.setBackgroundResource(R.drawable.valid_field);
                } else {
                    if (count > 0) {
                        other1.setBackgroundResource(R.drawable.invalid_field);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        other2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isPhoneValid(s.toString()) && count > 0) {
                    other2.setBackgroundResource(R.drawable.valid_field);
                } else {
                    if (count > 0) {
                        other2.setBackgroundResource(R.drawable.invalid_field);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> otherNumbers = new ArrayList<>();

                String fullName = name.getText().toString();
                String phone1 = other1.getText().toString();
                String phone2 = other2.getText().toString();

                if (!(fullName.trim().length() > 1)) {
                    Toast.makeText(FinalDetails.this, "Provide a valid name", Toast.LENGTH_SHORT).show();
                } else if (!isPhoneValid(phone1) && !isPhoneValid(phone2)) {
                    Toast.makeText(FinalDetails.this, "Provide at least one valid number", Toast.LENGTH_SHORT).show();
                } else {
                    if (isPhoneValid(phone1)) {
                        phone1 = standardTZNumber(phone1);
                        otherNumbers.add(phone1);
                    }

                    if (isPhoneValid(phone2)) {
                        phone2 = standardTZNumber(phone2);
                        otherNumbers.add(phone2);
                    }

                    DialogUtils.getInstance().startProgress(FinalDetails.this, "Please wait...");

                    WickerUser user = new WickerUser()
                            .setPhoneNumber(getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE)
                                    .getString("phoneNum", ""))
                            .setAltPhone1(phone1)
                            .setAltPhone2(phone2)
                            .setName(fullName);
                    if (hasPic) {
                        user.setBase64Image(getStringImage(avatar));
                    }

                    Backend.getInstance().registerUser(user, new RegistrationCallback() {
                        @Override
                        public void onRegistrationComplete(String message, Exception e) {
                            if (e == null) {
                                getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
                                        .edit().putBoolean("regCompleted", true).apply();

                                Answers.getInstance().logSignUp(new SignUpEvent().putSuccess(true));

                                DialogUtils.getInstance().stopProgress();

                                Toast.makeText(FinalDetails.this, message, Toast.LENGTH_SHORT).show();

                                startActivity(new Intent(FinalDetails.this, Initializing.class));
                                FinalDetails.this.finish();
                            } else {
                                DialogUtils.getInstance().stopProgress();
                                Log.d(getClass().getSimpleName(), "[+] Registration failed", e);
                            }
                        }
                    });
                }

            }
        });
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bmap = Bitmap.createScaledBitmap(bitmap, 480, 480, false);
        bmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);

        byte[] bytes = baos.toByteArray();

        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.i("URI Cropped", resultUri.getPath());

                try {
                    String filePath = resultUri.getPath();
                    File image = new File(filePath);
                    byte[] imageBytes = FileUtils.read(image);

                    File file = new File(getFilesDir(), "pics");
                    if (file.mkdirs()) {
                        File imageFile = new File(file, "avatar".concat(image.getName().substring(image.getName().lastIndexOf("."))));
                        FileUtils.write(imageFile, imageBytes);
                        if (imageFile.exists()) {
                            getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE)
                                    .edit()
                                    .putString("filePath", imageFile.getAbsolutePath()).apply();
                            Log.d(getClass().getSimpleName(), "[+] Avatar saved :".concat(imageFile.getAbsolutePath()));
                        }
                    }
                } catch (IOException e) {
                    Log.d(getClass().getSimpleName(), "[+] Saving failed", e);
                }

                try {
                    avatar = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    userPic.setImageBitmap(avatar);
                    hasPic = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }

        } else if (requestCode == 2123 && data != null) {
            Log.i("URI", data.getData().getPath());
            CropImage.activity(data.getData()).setCropShape(CropImageView.CropShape.OVAL).setAspectRatio(1, 1).setFixAspectRatio(true).start(FinalDetails.this);
        }

    }

}
