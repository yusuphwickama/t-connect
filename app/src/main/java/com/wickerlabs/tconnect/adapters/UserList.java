package com.wickerlabs.tconnect.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wickerlabs.tconnect.R;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.ContactsUtils;
import com.wickerlabs.tconnect.backend.Friend;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by: WickerLabs.Inc
 * Project title: HaloConnect
 * Time: 9:17 AM
 * Date: 8/3/2016
 * Website: http://www.wickerlabs.com
 */
public class UserList extends ArrayAdapter<Friend> {

    public static final int REQ_CODE = 765;
    private List<Friend> userList;
    private int resourceID;
    private Button save;
    private Activity activity;

    public UserList(Activity activity, int resource, List<Friend> users) {
        super(activity.getBaseContext(), resource, users);
        this.userList = users;
        this.resourceID = resource;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Friend getItem(int position) {
        return userList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    @SuppressLint("ViewHolder")
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());

        final View row = inflater.inflate(resourceID, parent, false);

        TextView fullName = (TextView) row.findViewById(R.id.listFullName);
        TextView halNum = (TextView) row.findViewById(R.id.listHalNum);
        CircleImageView userAvatar = (CircleImageView) row.findViewById(R.id.listAvatar);
        save = (Button) row.findViewById(R.id.listSave);

        final Friend friend = userList.get(position);

        halNum.setText(friend.getPhoneNum());
        final String displayName = ContactsUtils.findNameByNumber(getContext(), friend);
        fullName.setText(displayName);

        if (friend.getUid().equals(Backend.getInstance().getCurrentUser().getUid())) {
            save.setEnabled(false);
            save.setVisibility(View.INVISIBLE);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save = (Button) v;
                Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, displayName);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, friend.getPhoneNum());

                if (intent.resolveActivity(activity.getPackageManager()) != null) {
                    activity.startActivityForResult(intent, REQ_CODE);
                }
            }
        });

        if (friend.getAvatar() != null & friend.getAvatar().length() > 5) {
            try {
                Glide.with(getContext()).load(friend.getAvatar()).fitCenter().crossFade().into(userAvatar);
            } catch (Exception e) {
                Log.d(getClass().getSimpleName(), "Avatar failed: ".concat(friend.getAvatar()), e);
            }
        } else {
            Glide.with(getContext()).load(R.drawable.user).into(userAvatar);
        }

        if (ContactsUtils.isInPhonebook(getContext(), friend.getPhoneNum())) {
            save.setBackgroundResource(R.drawable.next_button_done);
            save.setText("Saved");
            save.setEnabled(false);
        }

        return row;
    }

}
