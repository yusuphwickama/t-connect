package com.wickerlabs.tconnect;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.bumptech.glide.Glide;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.WickerUser;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class AccountSettings extends AppCompatActivity {

    WickerUser mUser;
    CircleImageView userPic;
    Switch status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        mUser = Backend.getInstance().getCurrentUser();

        userPic = (CircleImageView) findViewById(R.id.userPicSettings);
        status = (Switch) findViewById(R.id.switch1);

        status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                status = (Switch) buttonView;
                if (isChecked) {
                    status.setText(status.getTextOn());
                } else {
                    status.setText(status.getTextOn());
                }
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                getUserImage(mUser);
            }
        }).start();
    }

    private void getUserImage(WickerUser user) {
        Bitmap bitmap = user.getImage();
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] data = baos.toByteArray();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Glide.with(AccountSettings.this).load(data).fitCenter().into(userPic);
                }
            });

        } else {
            Log.d(getClass().getSimpleName(), "No profile pic available");
        }
    }
}
