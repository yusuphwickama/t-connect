package com.wickerlabs.tconnect;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.digits.sdk.android.Digits;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.util.DialogUtils;

import io.fabric.sdk.android.Fabric;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 10:19 AM
 * Date: 7/27/2016
 * Website: http://www.wickerlabs.com
 */
public class AppStarter extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "AqNfPA5HwRkcqrtmJS4PyPHZv";
    private static final String TWITTER_SECRET = "B7x6p7DctZvqHgdUYGY48UjuOpHgRrF1HXT9PoS9oreEisQu9t";


    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Answers(), new Crashlytics(), new TwitterCore(authConfig), new Digits.Builder().build());

        DialogUtils.init();
        Backend.with(this);
    }
}
