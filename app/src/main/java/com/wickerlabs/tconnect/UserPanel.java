package com.wickerlabs.tconnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.PurchaseEvent;
import com.wickerlabs.tconnect.adapters.UserList;
import com.wickerlabs.tconnect.backend.Backend;
import com.wickerlabs.tconnect.backend.WickerUser;
import com.wickerlabs.tconnect.fragments.UserFriendsFragment;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Currency;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserPanel extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView username, halNumber;
    View mHeaderView;
    CircleImageView userAvatar;
    String TAG = "T-Connect";

    Fragment userFriends;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Drawable logo = getResources().getDrawable(R.drawable.ttcl_logo);
        //logo.setColorFilter(getResources().getColor(android.R.color.black), PorterDuff.Mode.SRC_IN);

        //getSupportActionBar().setLogo(logo);
        getSupportActionBar().setTitle("T-Connect");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mHeaderView = navigationView.getHeaderView(0);

        username = (TextView) mHeaderView.findViewById(R.id.mainUserName);
        halNumber = (TextView) mHeaderView.findViewById(R.id.mainHalNum);
        userAvatar = (CircleImageView) mHeaderView.findViewById(R.id.avatar);

        userFriends = getSupportFragmentManager().findFragmentById(R.id.haloFrag);

        if (Backend.getInstance().getCurrentUser() != null) {

            String user = Backend.getInstance().getCurrentUser().getName();

            getUserImage(Backend.getInstance().getCurrentUser());

            username.setText(user);
            halNumber.setText(Backend.getInstance().getCurrentUser().getPhoneNumber());
        }

        getSupportFragmentManager().beginTransaction()
                .show(userFriends)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }


    private void getUserImage(WickerUser user) {
        Bitmap bitmap = user.getImage();
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] data = baos.toByteArray();

            Glide.with(this).load(data).fitCenter().into(userAvatar);

        } else {
            Log.d(getClass().getSimpleName(), "No profile pic available");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_panel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            UserPanel.this.finish();
            return true;
        } else if (id == R.id.action_notify) {
            ProgressDialog dialog = new ProgressDialog(UserPanel.this);
            dialog.setCancelable(true);
            dialog.setMessage("Hello, world");
            dialog.show();

            Toast.makeText(UserPanel.this, "Still a work in progress.", Toast.LENGTH_SHORT).show();
            //sendPushNotification();
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_friends) {

            getSupportFragmentManager().beginTransaction()
                    .show(userFriends)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(UserPanel.this, AccountSettings.class));

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UserList.REQ_CODE && resultCode == RESULT_OK && data != null) {

            UserFriendsFragment.userListAdapter.notifyDataSetChanged();
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putCurrency(Currency.getInstance("USD"))
                    .putItemName("Contact number")
                    .putItemType("Service")
                    .putCustomAttribute("UserID", Backend.getInstance().getCurrentUser().getUid())
                    .putItemPrice(BigDecimal.valueOf(1.99))
                    .putSuccess(true));

            Answers.getInstance().logCustom(new CustomEvent("SaveNumber"));

        } else if (requestCode == UserList.REQ_CODE && resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Saving cancelled", Toast.LENGTH_SHORT).show();
        }
    }
}
