package com.wickerlabs.tconnect.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.ContextThemeWrapper;

import com.wickerlabs.tconnect.R;

/**
 * Created by: WickerLabs.Inc
 * Project title: T-Connect
 * Time: 12:19 AM
 * Date: 2/13/2017
 * Website: http://www.wickerlabs.com
 */

public class DialogUtils {

    private static ProgressDialog dialog;
    private static DialogUtils instance;

    private DialogUtils() {

    }

    public static void init() {
        instance = new DialogUtils();
    }

    public static DialogUtils getInstance() {
        return instance;
    }

    public void startProgress(Context context, String message) {
        dialog = new ProgressDialog(new ContextThemeWrapper(context, R.style.myDialog));
        dialog.setMessage(message);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        dialog.show();
    }

    public void stopProgress() {
        dialog.dismiss();
    }
}
